export const state = () => ({
  app_version: "1.0.0",
  access_token: ""
});

export const mutations = {};

export const actions = {
  async getIP({}) {
    const ip = await this.$axios.$get("http://icanhazip.com");
    return { ip };
  }
};

export const getters = {
  appVersion: state => state.app_version,
  accessToken: state => state.access_token
};
