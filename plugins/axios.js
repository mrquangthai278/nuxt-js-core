export default function({ $axios, redirect, store, app, route }) {
  const token = store.getters.accessToken;
  if (token) {
    $axios.setToken(store.getters.accessToken, "Bearer");
  }
  $axios.onResponse(response => {});
}
